<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use QrCode;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
        // QrCode::backgroundColor(255, 0, 0);
        // return Storage::get('app/user_img/default.png');
        // $img = '/public/default.png';
        // $img = QrCode::format('png')->merge('/public/irg.png')->size(300)->generate('awokaokwokokaokawokowakwaokwokaokwokawokwokaowkowakoakwo');
        // Storage::disk('public')->put('/barcode/img2.png', $img);
        // return url('public/storage/barcode/img2.png');
    }
}
