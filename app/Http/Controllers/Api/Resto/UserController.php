<?php

namespace App\Http\Controllers\Api\Resto;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\User;

class UserController extends Controller
{
    public function __construct() {
        $this->middleware('auth:api');
    }
    
    public function index(Request $request) {
        if (Auth::user()->roles_id == 4) {
            $owner = User::find(Auth::user()->id);
            $owner->name = $request->name;
            $owner->email = $request->email;
            $owner->phone_number = $request->phone_number;
            $owner->save();
            
            return response([
                'message' => 'Success',
                'status_code' => http_response_code()
            ]);
        } else {
            return response([
                'message' => 'Not found',
                'status_code' => http_response_code()
            ], 404);
        }
    }
}
