<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Bookmark;
use App\Menu;
use Auth;

class BookmarkController extends Controller
{
    public function __construct() {
        $this->middleware('auth:api');
    }

    public function index(Request $request) {
        if ($request->tab == 1) {
            $menu = Menu::with('promos:id,in_percent,menus_id', 'menu_types:id,name')->whereHas('bookmarks', function($query){
                return $query->where('users_id', Auth::user()->id);
            })->get();
        } else {
            $menu = Menu::with('promos:id,in_percent,menus_id', 'menu_types:id,name')->whereHas('bookmarks', function($query){
                return $query->where('users_id', Auth::user()->id);
            })->where('is_delivery', 1)->get();
        }
        

        return response([
            'menu' => $menu,
            'message' => 'Success',
            'status_code' => http_response_code()
        ]);
    }

    public function bookmark(Request $request) {
        $bookmarked = Bookmark::where('menus_id', $request->menus_id)->where('users_id', Auth::user()->id)->first();

        if (isset($bookmarked)) {
            Bookmark::find($bookmarked->id)->delete();
        } else {
            $bookmark = new Bookmark;
            $bookmark->users_id = Auth::user()->id;
            $bookmark->menus_id = $request->menus_id;
            $bookmark->save();
        }
        
        return response([
            'message' => 'Success',
            'status_code' => http_response_code()
        ]);
    }

    public function unbookmark() {

    }
}
