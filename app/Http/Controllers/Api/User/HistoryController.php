<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Transaction as Trans;
use App\TransactionDetail as Detail;
use App\Reservation;

class HistoryController extends Controller
{
    public function __construct() {
        $this->middleware('auth:api');
    }

    public function index(Request $request) {
        if ($request->tab == 1) {
            $history = Trans::with('restaurants:id,name', 'chatrooms:id,transactions_id')->where('users_id', Auth::user()->id)->where('is_done', 1)->get();
        } else {
            $history = Reservation::with('restaurants:id,name', 'chatrooms:id,reservations_id')->where('users_id', Auth::user()->id)->where('is_done', 1)->get();
        }

        return response([
            'history' => $history,
            'message' => 'Success',
            'status_code' => http_response_code()
        ]);
    }

    public function detail($id) {
        $history = Detail::with('menus:id,name,price,img')->where('transactions_id', $id)->get();
        return response([
            'history' => $history,
            'message' => 'Success',
            'status_code' => http_response_code()
        ]);
    }
}
