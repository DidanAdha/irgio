<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Menu;
use App\Promo;

class PromoController extends Controller
{
    public function __construct() {
        $this->middleware('auth:api');
    }

    public function index(Request $request) {
        if ($request->tab == 1) {
            $promo = Menu::with('promos:id,in_percent,menus_id', 'menu_types:id,name')->whereHas('promos', function($query){
                return $query->where('is_active', 1);
            })->get();
        } else {
            $promo = Menu::with('promos:id,in_percent,menus_id', 'menu_types:id,name')->whereHas('promos', function($query){
                return $query->where('is_active', 1);
            })->where('is_delivery', 1)->get();
        }

        return response([
            'promo' => $promo,
            'message' => 'Success',
            'status_code' => http_response_code()
        ]);
    }
}
