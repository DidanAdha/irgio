<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public function restaurants() {
        return $this->belongsTo(Restaurant::class);
    }

    public function users() {
        return $this->belongsTo(User::class);
    }

    public function chatrooms() {
        return $this->hasOne(Chatroom::class, 'transactions_id');
    }

    public function transaction_details() {
        return $this->hasMany(TransactionDetail::class, 'transactions_id');
    }
}
