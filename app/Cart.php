<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    public function menus() {
        return $this->belongsTo(Menu::class);
    }

    public function users() {
        return $this->belongsTo(User::class);
    }

    public function restaurants() {
        return $this->belongsTo(Restaurant::class);
    }
}
