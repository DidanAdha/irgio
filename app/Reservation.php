<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    public function restaurants() {
        return $this->belongsTo(Restaurant::class);
    }

    public function users() {
        return $this->belongsTo(User::class);
    }

    public function chatrooms() {
        return $this->hasOne(Chatroom::class, 'reservations_id');
    }
}
