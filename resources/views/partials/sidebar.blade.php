<aside id="sidebar-wrapper">
    <div class="sidebar-brand">
        <a href="{{ route('home') }}">IRG ADMIN</a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
      <a href="{{ route('home') }}">IRG</a>
    </div>
    <ul class="sidebar-menu">
        <li class="menu-header">Main</li>
          
            <li class="active"><a class="nav-link" href="index-0.html"><i class="fas fa-fire"></i><span>Dashboard</span></li>
        </li>
       
      </ul>

      <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
        <a href="https://getstisla.com/docs" class="btn btn-primary btn-lg btn-block btn-icon-split">
          <i class="fas fa-rocket"></i> Documentation
        </a>
      </div>
  </aside>